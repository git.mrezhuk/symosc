from selenium.webdriver import Firefox
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

from selenium.webdriver.firefox.options import Options

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

from selenium import webdriver
import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

from webdriver_manager.firefox import GeckoDriverManager

from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import random as rd
import json

import datetime as dt

from lib.csgolib import *

#print(utilsCs.parse(["csssel_test_test", "..."]))


# TODO ADD buttons and other tests


def TestingSiteCsgo(webdrivers, data):
    testing_results = {}

    testingdata = data['testing']
    for testingspace in testingdata:
        testing_results[testingspace] = {}
        for i in data[testingspace].items():
            trigger = True
            testing_name, click, testing_item = utilsCs.parse(i)
            try:
                e = webdrivers[testingspace].find_element(*testing_item)
                if(click):
                    e.click()
                    time.sleep(1)
                
            except Exception as e:
                #print(e.with_traceback())
                print(click, testing_item)
                trigger = False
            finally:
                testing_results[testingspace][testing_name] = trigger
    return testing_results


if __name__ == "__main__":

    url = "https://csgo3.run/double"
    profile_path = "docker-compose-test/settings-data/firefox-profiles-testing/"

    options = Options()
    #options.headless = True
    options.add_argument("-profile")
    options.add_argument(profile_path + "bet")
    bet_driver = webdriver.Firefox(options=options)

    bet_driver.set_window_position(0, 0)
    bet_driver.set_window_size(400, 800)

    options = Options()
    #options.headless = True
    options.add_argument("-profile")
    options.add_argument(profile_path + "shop")

    shop_driver = webdriver.Firefox(options=options)

    shop_driver.set_window_position(500, 0)
    shop_driver.set_window_size(1200, 800)

    bet_driver.get(url)
    shop_driver.get(url)
    print("sleep 6 sec")
    time.sleep(6)

    with open("docker-compose-test/settings-data/elements.json") as fp:
        data = json.load(fp)
        tmp = dt.datetime.now().timestamp()
        results = TestingSiteCsgo({"bet": bet_driver, "shop": shop_driver}, data)
        results.update({"testingInfo": {"timestamp": float(tmp), "timestampFriendly": str(dt.datetime.fromtimestamp(tmp))}})
        with open("docker-compose-test/settings-data/testing-results.json", "w") as f:
            json.dump(results, f)
    
    bet_driver.close()
    shop_driver.close()