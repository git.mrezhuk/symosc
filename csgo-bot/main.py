from lib.csgoliblog import *
from lib.strategies import *
import os
import time
import json

if __name__ == "__main__":
    time.sleep(10)

    with open(os.environ.get("ELEMENTS_JSON_PATH", "docker-compose-test/settings-data/elements.json"), "r") as fp:
        rdata = json.load(fp)

    data = {}
    for k, i in rdata.items():
        if(k == "testing"):
            continue
        data[k] = {}
        for j in i.items():
            name, click, d = utilsCs.parse(j)
            data[k].update({name: d})
    #print(data)
    #print(data["shop"]["changeButton"])

    l = logger("logging", STD_CONNECTION)
    try:
        c = logControler(os.environ.get("SITE_PATH", "https://csgo3.run/double"), l, data)
        try:
            s = Strategy2x(c)
            #s.bet = 3.44
            s.try_running_forever()
        except Exception as e:
            print(e)
        finally:
            c.bet_driver.close()
            c.shop_driver.close()
    except Exception as e:
        print(e)
    