from lib.csgoliblog import *
from lib.csgolib import *

class Strategy:
    def __init__(self):
        pass


class Strategy2x:
    def __init__(self, c):
        self.c = c
        self.min_bet = 0.2
        self.bet = self.min_bet
    
    def iteration(self):
        balance = self.c.get_balance_with_weapons()
        if(balance - 2 * self.bet < 0):
            self.bet = self.min_bet
        
        r = c.buy_weapon(self.bet)
        while(r != True):
            time.sleep(2)
            r = c.buy_weapon(self.bet)
        
        dif = c.bet_weapon(self.bet, rd.choice([c.BLUE, c.GREEN]))
        if(dif < 0):
            self.bet *= 2
        elif(dif > 0):
            self.bet = self.min_bet

        print(str(round(balance + round(dif, 2), 2)) + " " + str(round(dif, 2)))

    def run_forever(self):
        while(True):
            self.iteration()
    
    def run_count_iterations(self, n):
        for i in range(n):
            self.iteration()
    
    def try_running_forever(self):
        while(1):
            try:
                self.run_forever()
            except Exception as e:
                print(e)

class StrategyNx:
    def __init__(self, c:Controler):
        self.c = c
        # self.bet = 
