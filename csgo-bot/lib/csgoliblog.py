import lib.csgolib as cl
import pymongo
import os

STD_CONNECTION = 'mongodb://' + os.environ['MONGODB_USERNAME'] + ':' + os.environ['MONGODB_PASSWORD'] + '@' + os.environ['MONGODB_HOSTNAME'] + ':27017/' + os.environ['MONGODB_DATABASE']

class logger:
    def __init__(self, table, connection):
        self.client = pymongo.MongoClient(connection)
        self.db = self.client.db["table"]

    def log(self, item:dict):
        self.db.insert_one(item)


class logControler(cl.Controler):
    def __init__(self, url, lg:logger, profile_path="/tmp/firefox-profiles/", command_executor='http://selenium-hub:4444/wd/hub'):
        super().__init__(url, profile_path, command_executor)
        self.lg = lg

    def buy_weapon(self, cost):
        a = super().buy_weapon(cost)
        self.lg.log({"optype":"BUY_WEAPON", "cost":cost, "result":a})

    def bet_weapon(self, cost, sector_xpath):
        a = super().bet_weapon(cost, sector_xpath)
        self.lg.log({"optype":"BET_WEAPON", "cost":cost, "result":a})