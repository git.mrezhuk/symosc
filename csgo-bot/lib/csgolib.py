from selenium.webdriver import Firefox
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

from selenium.webdriver.firefox.options import Options

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

from selenium import webdriver
import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

from webdriver_manager.firefox import GeckoDriverManager

from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import random as rd

convertTable = {
    "xpath": By.XPATH,
    "name": By.NAME,
    "cssname": By.CLASS_NAME,
    "csssel": By.CSS_SELECTOR,
    "id": By.ID,
    "linktext": By.LINK_TEXT,
    "plinktext": By.PARTIAL_LINK_TEXT,
    "tagname": By.TAG_NAME
}

class utilsCs:
    def parse(array):
        ke = array[0].split("_", 1)
        it = array[1]
        
        name, ke = ke[1], ke[0]

        clk = False
        if(type(it) != str):
            clk = it.get("click", False)
            it = it["context"]

        ke0 = ke
        ke = convertTable.get(ke, None)
        if(ke == None):
            raise Exception("Undefined access type: \"" + ke0 + "\"")
        
        return name, clk, (ke, it)

class WeaponUtils:
    
    def find_by_cost(weapons, cost):
        for i in range(len(weapons)):
            if(weapons[i].get_cost() == cost):
                return weapons[i]
    
    def find_near_cost(weapons, cost):
        mn = 10**10
        w = None
        for i in range(len(weapons)):
            if(mn > abs(weapons[i].get_cost() - cost)):
                mn = abs(weapons[i].get_cost() - cost)
                w = weapons[i]
        return w

    def find_by_name(weapons, name):
        for i in range(len(weapons)):
            if(weapons[i].get_name() == name):
                return weapons[i]
        


    def select_all(weapons):
        for i in range(len(weapons)):
            weapons[i].select()
    
    def unselect_all(weapons):
        for i in range(len(weapons)):
            weapons[i].unselect()
    
    

class Weapon:
    def __init__(self, weapon_link):
        self.weapon_link = weapon_link
    
    def __str__(self):
        return self.get_name() + " " + self.get_other() + " " + self.get_rate() + " " + str(self.get_cost())

    def is_clicked(self):
        if("checked" in self.weapon_link.get_attribute("class")):
            return True
        else: 
            return False
    
    def click(self):
        self.weapon_link.click()

    def select(self):
        if not self.is_clicked():
            self.click()
    
    def unselect(self):
        if self.is_clicked():
            self.click()
        
    def get_name(self):
        return self.weapon_link.find_element(By.CLASS_NAME, "drop-preview__title").text
    
    def get_other(self):
        return self.weapon_link.find_element(By.CLASS_NAME, "drop-preview__subtitle").text

    def get_rate(self):
        return self.weapon_link.find_element(By.CLASS_NAME, "drop-preview__desc").text

    def get_cost(self):
        return float(self.weapon_link.find_element(By.CLASS_NAME, "drop-preview__price").text.replace("$", ""))

class Controler:

    def wait(driver, data, delay = 5):
        try:
            myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located(data))
            time.sleep(1)
            return myElem
        except TimeoutException:
            return None

    def __init__(self, url, site_elements, profile_path="/tmp/firefox-profiles/", command_executor='http://selenium-hub:4444/wd/hub'):

        self.site_elements = site_elements

        self.BLUE = site_elements["bet"]["blueSector"]
        self.GREEN = site_elements["bet"]["greenSector"]

        options = Options()
        options.add_argument("-profile")
        options.add_argument(profile_path + "bet")
        '''
        options.set_preference("browser.cache.disk.enable", False)
        options.set_preference("browser.cache.memory.enable", False)
        options.set_preference("browser.cache.offline.enable", False)
        options.set_preference("network.http.use-cache", False)
        options.set_preference("browser.download", True)
        '''
        self.bet_driver = webdriver.Remote(
            command_executor=command_executor,
            options=options
        )

        options = Options()
        options.add_argument("-profile")
        options.add_argument(profile_path + "shop")
        '''
        options.set_preference("browser.cache.disk.enable", False)
        options.set_preference("browser.cache.memory.enable", False)
        options.set_preference("browser.cache.offline.enable", False)
        options.set_preference("network.http.use-cache", False)
        options.set_preference("browser.download", True)
        '''
        self.shop_driver = webdriver.Remote(
            command_executor=command_executor,
            options=options
        )

        self.shop_driver.set_window_position(500, 0)
        self.shop_driver.set_window_size(1200, 800)

        self.bet_driver.get(url)
        self.shop_driver.get(url)

        Controler.wait(self.shop_driver, site_elements["shop"]["changeButton"])
        '''
        self.shop_driver.execute_script("""
            var l = document.getElementsByClassName("inner-page")[0];
            l.parentNode.removeChild(l);
        """)'''
		
        time.sleep(2)
		
        self.shop_driver.find_element(*site_elements["shop"]["changeButton"]).click()



    def get_balance(self):
        a = self.bet_driver.find_element(*self.site_elements["shop"]["balance"])

        return float(a.text[:-2])


    def get_all_weapons_in_shop(self):
        a = self.shop_driver.find_element(*self.site_elements["shop"]["weaponStorage"])
        a = a.find_elements(By.TAG_NAME, "button")

        r = []

        for i in range(len(a)):
            r.append(Weapon(a[i]))
        return r

    def get_all_weapons_in_bet(self):
        a = self.bet_driver.find_element(self.site_elements["bet"]["weaponStorage"])
        a = a.find_elements(By.TAG_NAME, "button")

        r = []

        for i in range(len(a)):
            r.append(Weapon(a[i]))
        return r
    
    def get_balance_with_weapons(self):
        o = 0
        for weapon in self.get_all_weapons_in_bet():
            o += weapon.get_cost()
        return self.get_balance() + o
    
    def buy_weapon(self, cost):

        l = self.get_all_weapons_in_shop()

        p = WeaponUtils.find_by_cost(l, cost)

        if(p):
            return True

        WeaponUtils.select_all(l)

        e = Controler.wait(self.shop_driver, self.site_elements["shop"]["costEntry"])
        if(e == None):
            return False
        e.clear()
        e.send_keys(str(cost), Keys.ENTER)
        time.sleep(1)

        e = Controler.wait(self.shop_driver, self.site_elements["shop"]["weaponShopContainer"])
        if(e == None):
            return False
        tmp = Weapon(e)
        if(tmp.get_cost() != cost):
            return False
        
        tmp.click()

        self.shop_driver.find_element(self.site_elements["shop"]["buyButtonInShop"]).click()
        return True

    def bet_weapon(self, cost, sector_xpath):
        l = self.get_all_weapons_in_bet()
        
        old_balance = self.get_balance_with_weapons()

        WeaponUtils.unselect_all(l)

        time.sleep(1)
        w = WeaponUtils.find_by_cost(l, cost)
        if(w == None):
            w = WeaponUtils.find_near_cost(l, cost)
            if(w == None):
                return 0
        time.sleep(2)
        w.select()
        time.sleep(1)
        

        WebDriverWait(self.bet_driver, 30).until(EC.element_to_be_clickable((By.XPATH, sector_xpath))).click()
        time.sleep(30)
        new_balance = self.get_balance_with_weapons()
        return new_balance - old_balance
