import os
from flask import Flask, request, jsonify, render_template
from flask_pymongo import PyMongo

import json


application = Flask(__name__, template_folder='templates')

#application.config["MONGO_URI"] = 'mongodb://' + os.environ['MONGODB_USERNAME'] + ':' + os.environ['MONGODB_PASSWORD'] + '@' + os.environ['MONGODB_HOSTNAME'] + ':27017/' + os.environ['MONGODB_DATABASE']

#mongo = PyMongo(application)
#db = mongo.db

@application.route('/')
def index():
    return jsonify(
        status=True,
        message='Welcome to the Dockerized Flask MongoDB app!'
    )

@application.route('/htmltable')
def tableTest():

    data = {}
    with open("docker-compose-test/settings-data/testing-rerults.json") as fp:
        data = json.load(fp)
    #print(data)


    r = list(data.items())
    for i in range(len(r)):
        r[i] = [r[i][0], list(r[i][1].items())]

    return render_template("table.html", data=r)

@application.route("/jsontable")
def jsonTable():
    data = {}
    with open("docker-compose-test/settings-data/testing-results.json") as fp:
        data = json.load(fp)
    
    return data
'''
@application.route('/todo')
def todo():
    _todos = db.todo.find()

    item = {}
    data = []
    for todo in _todos:
        item = {
            'id': str(todo['_id']),
            'todo': todo['todo']
        }
        data.append(item)

    return jsonify(
        status=True,
        data=data
    )

@application.route('/todo', methods=['POST'])
def createTodo():
    data = request.get_json(force=True)
    item = {
        'todo': data['todo']
    }
    db.todo.insert_one(item)

    return jsonify(
        status=True,
        message='To-do saved successfully!'
    ), 201
'''
if __name__ == "__main__":
    ENVIRONMENT_DEBUG = os.environ.get("APP_DEBUG", True)
    ENVIRONMENT_PORT = os.environ.get("APP_PORT", 5000)
    application.run(host='0.0.0.0', port=ENVIRONMENT_PORT, debug=ENVIRONMENT_DEBUG)

